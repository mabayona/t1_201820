package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.logic.IntegersBagOperations;

public class Controller {

	private static IntegersBagOperations model = new IntegersBagOperations();
	
	
	public static IntegersBag createBag(ArrayList<Integer> values){
         return new IntegersBag(values);		
	}
	
	
	public static double getMean(IntegersBag bag){
		return model.computeMean(bag);
	}
	
	public static int getMax(IntegersBag bag){
		return model.getMax(bag);
	}
	
	public static int getMin(NumbersBag bag){
		return model.getMin(bag);
	}
	
	public static int getSum(NumbersBag bag){
		return model.getTotalSum(bag);
	}
	
	public static int positiveNumbers(NumbersBag bag){
		return model.positiveNumbers(bag);
	}

}
